<?php

/**
 * @file
 * The pages/forms for the user_knock module.
 */

/**
 * User knock config page.
 */
function user_knock_config_page() {
  $knock = user_knock_secret_knock();

  $output = '';

  $output .= '<p>';
  $output .= l($knock, 'user/knock', array('query' => array('k' => $knock)));
  $output .= '</p>';

  return $output;
}

/**
 * The user knocked, check the knock and enable the superuser if it's correct.
 */
function user_knock_knock() {
  // If the knock is correct, unblock the superuser
  // and redirect to the login page.
  if (empty($_GET['k']) === FALSE && $_GET['k'] == user_knock_secret_knock()) {
    $notify = variable_get('user_mail_status_activated_notify');
    
    // Disable user activated status email notification,
    // and enable when done.
    variable_set('user_mail_status_activated_notify', FALSE);
    user_user_operations_unblock(array(1));
    variable_set('user_mail_status_activated_notify', $notify);

    drupal_goto('user/login');
  }

  return MENU_ACCESS_DENIED;
}

/**
 * Display a form on the /user page for sending the secret knock
 * to the super user.
 */
function user_knock_send_knock() {
  global $user;

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Admin username or e-mail address'),
    '#size' => 60,
    '#maxlength' => max(USERNAME_MAX_LENGTH, EMAIL_MAX_LENGTH),
    '#required' => TRUE,
  );
  // Allow logged in users to request this also.
  if ($user->uid > 0) {
    $form['name']['#type'] = 'value';
    $form['name']['#value'] = $user->mail;
    $form['mail'] = array(
      '#prefix' => '<p>',
      '#markup' =>  t('User knock instructions will be mailed to %email.', array('%email' => $user->mail)),
      '#suffix' => '</p>',
    );
  }
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('E-mail the knock'));

  return $form;
}

/**
 * Validate the form by checking the user exists and is the super user.
 */
function user_knock_send_knock_validate($form, &$form_state) {
  $name = trim($form_state['values']['name']);
  // Try to load by email.
  $account = user_load_by_mail($name);
  if (!$account) {
    // No success, try to load by name.
    $account = user_load_by_name($name);
  }
  if (isset($account->uid)) {
    if ($account->uid == 1) {
      form_set_value(array('#parents' => array('account')), $account, $form_state);
    }
    else {
      form_set_error('name', t('Sorry, %name is not recognized as the admin.', array('%name' => $name)));
    }
  }
  else {
    form_set_error('name', t('Sorry, %name is not recognized as a user name or an e-mail address.', array('%name' => $name)));
  }
}

/**
 * Send the super user an email with the secret knock.
 */
function user_knock_send_knock_submit($form, &$form_state) {
  $account = $form_state['values']['account'];

  drupal_mail('user_knock', 'knock', $account->mail, user_preferred_language($account), array('account' => $account));

  watchdog('user_knock', 'Knock mailed to %name at %email.', array('%name' => $account->name, '%email' => $account->mail));
  drupal_set_message(t('Further instructions have been sent to your e-mail address.'));

  $form_state['redirect'] = 'user';

  return;
}
